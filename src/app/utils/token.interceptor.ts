import { Injectable } from '@angular/core';
import axios from "axios";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { ApiService } from '../services/api.service';
import { Observable, of } from 'rxjs'
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: ApiService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.accessToken}`
      }
    });
    return next.handle(request);
  }
}