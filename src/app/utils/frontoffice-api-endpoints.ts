export class FrontOfficeApiEndPoints {
    public static get login(): string { return "front-desk-staff-login"; }
    public static get forgotpassword(): string { return "forgot-password"; }
    public static get verify(): string { return "front-desk-verify"; }
    public static get resetpassword(): string { return "front-desk-reset-password"; }
    public static get orderlist(): string { return "frontdesk/orders-list"; } 
    public static get OrderViewList(): string { return "frontdesk/order-view"; }   
    public static get orderlistsearch(): string { return "frontdesk/search-order-list"; }
    public static get changestatus(): string { return "frontdesk/change-status"; } 
    public static get logout(): string { return "frontdesk-logout"; }   
  }