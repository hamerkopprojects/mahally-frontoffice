import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Ng2CompleterModule } from 'ng2-completer';

@Component({
  selector: 'app-pending-orders',
  templateUrl: './pending-orders.component.html',
  styleUrls: ['./pending-orders.component.css']
})
export class PendingOrdersComponent implements OnInit {

  order_status:number;
  messageType = null;

  orderList = [];
  searchForm: FormGroup;
  isInitialLogin:boolean;
  AutoFillData=[];
  CurrentFilterValue:any;
  TimerCount:number=5;
  currentPage:any;
  totalItems:any;
  OrderitemsPerPage:number=10;
  doReload:boolean=true;
  langindex:any=0;
  isFIlterOn:any=0;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private SpinnerService: NgxSpinnerService
  ) {

   }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.searchForm = this.formBuilder.group({
      order_id: ['', [Validators.required]]
    });
    this.doFetchPendingOrderList();
    this.GetPendingDetails();
    const atoken =JSON.parse(localStorage.getItem('accessToken'))
    if(this.doReload==true)
   {
    setInterval( () => {
      this.TimerCount=this.TimerCount-1;
     if(this.TimerCount==0&& this.doReload==true)
      {
        this.doFetchPendingOrderList();
        this.TimerCount=5;    
      }
      if(this.doReload==false)
      {
        this.TimerCount=5;   
      }

    },1000);
   
  }
  }
 
ngOnDestroy(): void
{
  this.doReload=false;
}



  GetPendingDetails() {  
    this.SpinnerService.show();
     
    this.order_status=1;
    this.apiService.doGetOrderList(this.order_status)
      .subscribe(
        (data: any) => {

       //   this.orderList = data; 
          // console.log('SpinnerLoading Data');
          // console.log(data.data.orders);
          this.orderList =data.data.orders;
          
          this.SpinnerService.hide();
        }
      );
  }

  doResetsearch(){
    this.doReload=true;
    this.isFIlterOn=0;
    location.reload();

  }

  doFetchPendingOrderList() {
    // this.SpinnerService.show();
    this.order_status=1;
    this.apiService.doGetOrderList(this.order_status)
      .subscribe(
        (data: any) => {

          //  console.log('Initial Log Data');
          //  console.log(data.data.orders);
          this.orderList =data.data.orders;
          this.AutoFillData=data.data.orders.map(x=>x.order_id);
          this.totalItems=this.AutoFillData.length;
          // this.toastr.success('Order Fetched');
          //console.log(this.orderList[0].menu_item[0].lang[0].name);
          //this.SpinnerService.hide();
        },
        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
          //this.SpinnerService.hide();
        }
      );
      }
  
      doFetchOrderListByOrderid() {
        this.doReload=false;
        this.isFIlterOn=1;
        this.SpinnerService.show(); 
        let data: any = this.searchForm.value;
       // Lintothis.apiService.doGetOrderListByOrderid(data.order_id)
        
        this.apiService.doGetOrderListByOrderid(this.CurrentFilterValue)
          .subscribe(
            (data: any) => {
              this.orderList =data.data.orders;
              this.totalItems=data.data.orders.length;
              // this.toastr.success('Order Fetched');
               //console.log(data.data.orders);
              this.SpinnerService.hide();
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error);
              this.SpinnerService.hide();
            }
          );
          }    
          
      doBump(order_id : number){
        this.router.navigate(['/bump'],{ queryParams: { order_id: order_id,order_status:this.order_status,routerLink:'pending_orders' }});
      }   
      
     doViewDetails(order_id : number){
      this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: order_id,order_status:this.order_status,redirectoPage:'porders' }});
     } 
     onPageChange(number: number) {
      // this.logEvent(`pageChange(${number})`);
      // this.config.currentPage = number;
      console.log('page index change');
      this.currentPage=number;
  }
}
