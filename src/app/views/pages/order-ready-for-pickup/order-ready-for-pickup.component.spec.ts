import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderReadyForPickupComponent } from './order-ready-for-pickup.component';

describe('OrderReadyForPickupComponent', () => {
  let component: OrderReadyForPickupComponent;
  let fixture: ComponentFixture<OrderReadyForPickupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderReadyForPickupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReadyForPickupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
