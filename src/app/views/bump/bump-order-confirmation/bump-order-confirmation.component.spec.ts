import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BumpOrderConfirmationComponent } from './bump-order-confirmation.component';

describe('BumpOrderConfirmationComponent', () => {
  let component: BumpOrderConfirmationComponent;
  let fixture: ComponentFixture<BumpOrderConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BumpOrderConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BumpOrderConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
